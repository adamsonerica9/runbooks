<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Packagecloud Service

* [Service Overview](https://dashboards.gitlab.net/d/packagecloud-main/packagecloud-overview)
* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22packagecloud%22%2C%20tier%3D%22inf%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::PackageCloud"

## Logging

* [ops](https://nonprod-log.gitlab.net/app/r/s/xBFHH)
* [pre](https://nonprod-log.gitlab.net/app/r/s/5ATui)

## Troubleshooting Pointers

* [PackageCloud Infrastructure and Backups](infrastructure.md)
* [Re-indexing a package](reindex-package.md)
* [GPG Keys for Package Signing](../packaging/manage-package-signing-keys.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
